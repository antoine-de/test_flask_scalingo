from flask import Flask, request, jsonify, url_for
from werkzeug.middleware.proxy_fix import ProxyFix

def create_app():
    app = Flask(__name__)

    app.wsgi_app = ProxyFix(
        app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1
    )
    
    
    @app.route("/headers")
    def headers():
        return jsonify(headers=dict(request.headers))
    @app.route("/url")
    def url():
        return jsonify(url=url_for('headers', _external=True))
    
    return app
